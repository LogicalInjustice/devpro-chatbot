<?php

class clientPackets
{
        public $GameList = 0;
        public $RemoveRoom = 1;
        public $UpdatePlayers = 2;
        public $LoginAccepted = 3;
        public $LoginFailed = 4;
        public $ServerMessage = 5;
        public $Banned = 6;
        public $Kicked = 7;
        public $RegisterAccept = 8;
        public $RegisterFailed = 9;
        public $Pong = 10;
        public $RoomStart = 11;
        public $UserList = 12;
        public $UpdateUserInfo = 18;
        public $FriendList = 15;
        public $JoinChannelAccept = 16;
        public $Message = 17;
        public $DuelRequest = 13;
        public $DevPoints = 19;
        public $UserStats = 20;
        public $TeamStats = 21;
        public $TeamList = 22;
        public $AcceptDuelRequest = 23;
        public $RefuseDuelRequest = 24;
        public $StartDuel = 25;
        public $TeamRequest = 26;
        public $GameServers = 29;
        public $RemoveServer = 30;
        public $AddServer = 31;
        public $ChannelList = 32;
        public $TournamentList = 33;
        public $TournamentRoomList = 34;
        public $TournamentRommUpdate = 35;
        public $TournamentMessage = 36;
        public $CreateRoom = 37;
        public $ChannelUsers = 38;
        public $AddChannelUser = 39;
        public $RemoveChannelUser = 40;
        public $Ranking = 41;
        public $Invalid = 42;
        public $InvalidTemp = 99;
        public $ValidateAccept = 43;
        public $ValidateFailed = 44;
        public $ResendAccept = 45;
        public $ResendFailed = 46;
        public $ChangeAccept = 47;
        public $ChangeFailed = 48;
        public $DuplicateMail = 49;
        public $BlacklistMail = 50;
        public $MailFormat = 51;
        public $QueueFail = 52;
        public $MatchFound = 53;
        public $MatchCanceled = 54;
        public $MatchStart = 55;
    
}