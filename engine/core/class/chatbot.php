<?php
require 'serverPackets.php';

class chatbot extends sockets
{
      // Response Bot Messages, wird in Datenbank ausgelagert bei
      public $chatbotResponseArray = array(
            "bot" => array(
                0 => "Hello my Friend!",
                1 => "Hatten wir schon die freude ?",
                2 => "Ich hab gerade keene Zeit, sry",
                3 => "Wie kann ich dir helfen ?",
                4 => "Hi",
                5 => "Ist Dir langweilig ?",
                6 => "Bonjour",
                7 => "Hello",
                8 => "Ist heute nicht ein guter Tag!",
                9 => "Guten Tag"),
            "chatbot talk" => "Ich habe aktuell noch keine Hilfe Funktion.",
            "chatbot go" => "Laufen muss ich erst noch lernen",
            "chatbot sit" => "Keine Lust jetzt zu sitzen, frag später nochmal nach!",
			"bot duell" => "Tut mir leid ich bin beschränkt, spiel doch mit meinem grossen Bruder dem DevBot!"
        );
      
        public $chatbotautotalk = array(
            "chatbot" => array(
                0 => "Hello my Friend!",
                1 => "I am Bot on autotalking",
                2 => "this is a test",
                3 => "sry for troubling",
                4 => "game over",
                5 => "i want talk",
                6 => "you wann play with me?",
                7 => "go out of my head",
                8 => "no, stop that please",
                9 => "be or not to be",
                10 => "i cannot sleep, im a bot :/",
                11 => "lets have some fun!",
                12 => "not with me my friend!",
                13 => "hey benblub you are my friend?",
                14 => "lets starting talk!",
                15 => "lol",
                16 => "sry i gone to sleep! bye bye"
                )
        );

        public $usernameArray = [];


        public function ChatbotResponseArray($socket, $message, $from)
            {

            $_SESSION['userArray'] = $from;

                // if Message first char is ! use Talk Functions
                $first_char_from_msg = substr($message, 0, 1);
                if($first_char_from_msg == '!')
                    {
                        $talkResponse = new talkResponse();
                        $response = $talkResponse->detectRequest($message);

                    //$rand = rand(1, 9);
                    $this->responseAnyMessage($socket, $response);
                    }
               else
               {
                        switch ($message)
                        {
                            //case 'chatbot stats': $this->chatbotSendCommand($socket, "STATS"); break;
                            //case 'chatbot uptime': $this->chatbotSendCommand($socket, "UPTIME"); break;
                            //case 'chatbot getDevpoints': $this->playRoulette($socket, "ADDPOINTS"); break;
                            //case 'devpoints pls ich bin auch lieb': $this->playRoulette($socket, "ADDPOINTS", $from); break;
                        }
               }   
        }
        
        public function ChatbotResponseArrayToUser($socket, $message)
            {        
                if($message == "chatbot")
                    {
                    $rand = rand(1, 9);
                    return $this->chatbotResponseArray['chatbot'][$rand];
                    }
            }
    /*
     * SendCommand
     */
    public function chatbotSendCommand($socket, $command) {
        $array = array( 
                    "Command"=>$command,
                    "Data"=>""
                    );
        $json_data = json_encode($array);
           
        $type = new serverPackets();
        $this->SocketSendData($socket, $type->ChatCommand, $json_data);    
    }
    
    /*
     * Sends Login Data from Config
     */
    public function sendLogin($socket, $username, $password) {
            $array = array(  
                "Username"=>$username,
                "Password"=>$password,
                "UID"=>"test",
                "Version"=>"210011"
            );
           $json_data = json_encode($array);
           $type = new serverPackets();
                
            // login
            $this->SocketSendData($socket, $type->Login, $json_data);             
    }
    
    public function joinChannel($socket, $channel) {
                   $type = new serverPackets();
                   $utf_channel = utf8_encode($channel);
                   
                    // join german chat
                    $this->SocketSendData($socket, $type->JoinChannel, $utf_channel);         
                   
                   //$this->responseAnyMessage($socket, "Grüsse an alle Spieler,");
    }
    
     public function responseAnyMessage($socket, $text) {
                    // protect Time Response
                    if(!isset($_SESSION["startTimer"]))
                    {
                            $_SESSION["startTimer"] = time();
                            $_SESSION["stopTimer"] = 8;
                    }
                    else
                        {
                            $_SESSION["stopTimer"] = time() - $_SESSION["startTimer"];
                            $_SESSION["startTimer"] = time();
                    }
					
			if($_SESSION["stopTimer"] > 5)
                        {
                            // send welcome msg ChatMessage = 8
                             $array = array(  
                             "message"=>$text,
                             "channel"=>CHANNEL,
                             "from"=>USERNAME,
                             "type"=>1,
                             "command"=>0  
                             );

                            $json_data = json_encode($array);

                            $type = new serverPackets();
                            $this->SocketSendData($socket, $type->ChatMessage, $json_data);
                        }
                    //sleep(1); // Protection that the Bot dont run in a Mute from System
    }
    
    public function terror($socket, $text) {
                    // protect Time Response
                    
                            // send welcome msg ChatMessage = 8
                             $array = array(  
								 "message"=>$text.' DEBUG: '.$_SESSION["stopTimer"],
								 "channel"=>CHANNEL,
								 "from"=>USERNAME,
								 "type"=>1,
								 "command"=>0  
                             );

                            $json_data = json_encode($array);

                            $type = new serverPackets();
                            $this->SocketSendData($socket, $type->ChatMessage, $json_data);
                        
                    sleep(3); // Protection that the Bot dont run in a Mute from System
    }
    
    /*
     * FUN_test
     */
    public function autotalk($socket) {
        $rand = rand(1, 15);
        $_SESSION['autotalk'] = 1;
        if($_SESSION['autotalk'] < 10){
            $this->terror($socket, $this->chatbotautotalk['chatbot'][$rand]);
            $_SESSION['autotalk']++;
        }
        if($_SESSION['autotalk'] == 10){
            $this->terror($socket, '/me '.$this->chatbotautotalk['chatbot'][16]);
        }
    }
    
    public function responseToUserMsg($socket, $text, $channel) {
        
        /* $array = array(  
                    "message"=>$text,
                    "channel"=>$channel,
                    "from"=>"Chatbot",
                    "type"=>3,
                    "command"=>0  
                    );
                    
                   $json_data = json_encode($array);
                  
                   $type = new serverPackets();
                   $this->SocketSendData($socket, $type->ChatMessage, $json_data);
                   
                    sleep(1); // Protection that the Bot dont run in a Mute from System
         * */
         
    }
    
    /*
     * Hier type, data, from
     */
    public function loadChatResponse($socket, $buffer) {
        
        $cut = substr("$buffer", 3);
        $jsonData = json_decode($cut);
        if(is_object($jsonData))
        {
            if(isset($jsonData->type))
            {
                if(isset($jsonData->from->username) && $jsonData->from->username == AUTOKICK_USERNAME_CHATRESPONSE){
                    $this->kickUser($socket, 18, $jsonData->from->username);
                }
                switch ($jsonData->type) {
                    case 1: // Response to joined Chat
                        $this->ChatbotResponseArray($socket, $jsonData->message, $jsonData->from->username);
                        break;
                    case 2: // Response Server Message in Chat
                        $this->responseAnyMessage($socket, $jsonData->message);
                        break;
                    case 3: // Response to User
                        //var_dump($jsonData);
                        $text = $this->ChatbotResponseArrayToUser($socket, $jsonData->message);
                        $this->responseToUserMsg($socket, $text, $jsonData->channel);
                        break;
                    default:
                        break;
                }
            }
       
        }
        
    }
    
    /*
     * incoming ServerMessage
     * utf_decode incoming data!
     */
    public function ServerMessage($buffer) {
        /*$data = utf8_encode($buffer);
        echo 'INCOMING ServerMessage: ';
        echo "<br>";
        print_r($data); */
    }
    
    /*
     * incoming DuelRequest
     * utf_8_encode incoming Data
     */
    public function DuelRequest($socket, $buffer) {
        $cut = substr("$buffer", 3);
        $jsonData = json_decode($cut);
        //echo "From: ".$jsonData->username."\n";
        //echo "DuelFormatString: ".$jsonData->duelformatstring."\n";
        
        
        $type = new serverPackets();
        $this->responseAnyMessage($socket, "Duelmodus Offline, do duel request to DevBot ".$jsonData->username);
        
        //Autorefuse DuelRequest
        $this->SocketSendCommand($socket, $type->RefuseDuel);
        //$this->SocketSendCommand($socket, $type->AcceptDuel);

    }
    
    public function startDuel($socket, $duelformatstring, $server) {
       // new socket Connection to Duelserver
       $this->onOpen(IP,DUELLSERVER_PORT); // 8911 ip and port stored in config.php
        // endless Loop, Listen Events 
        while(true) {
           $this->onMessageDuelServer();
        }
       //the whole loadClientPacketsType function is different
        
      //the onMessage function shouldn't try to get the json  
    }
    
    public function startDuel2($socket, $duelformatstring, $server) {
        
        $array = array(  
                "Command"=>$server,
                "Data"=>$duelformatstring
            );
           $json_data = json_encode($array);
           $this->SocketSendData($socket, $type->HostDuel, $json_data);
           sleep(1);
           $type = new serverPackets();
           $this->SocketSendData($socket, $type->HostDuel, $json_data);
    }
    public function createRoom($socket, $duelformatstring, $server) {
        $array = array(  
                "Command"=>$server,
                "Data"=>$duelformatstring
            );
    }
    /*
     * Hier können Spieler 1x pro Stunde um Devpoints spielen.
     * Es sollen zwischen -50 und 100 Punkte gespielt werden können!
     */
    // $command = "POINTS"
    // $data = {"addpoints":"100","username":"benblub"}
    public function playRoulette($socket, $command, $from) {
        
        $this->checkUserForRoulette($from); // check user has played
 
        // msg an Chat das Roulette gespielt wird
        $this->responseAnyMessage($socket, "Lass uns Roulette spielen $from, dein Einsatz: 30 Devpoints!");
        sleep(1);
        
        $rand = rand(-1, 10);
        $array = array( 
                    "Command"=>$command,
                    "Data"=>"$from $rand"
                    );
        $json_data = json_encode($array);
           
        $type = new serverPackets();
        $this->SocketSendData($socket, $type->ChatCommand, $json_data);
 
        
    }
    
    public function checkUserForRoulette($fromUsername) {
        
        // Zur DB verbinden
        $db = $this->openDatabase();
        
        // prüfe ob Username exist
        $query = ("SELECT dev_user FROM user WHERE dev_user = ?");
        $eintrag = $db->prepare($query);
        $eintrag->bindParam(1, $fromUsername);
        $eintrag->execute();
        
         while ($zeile = $eintrag->fetch())
        {
            $is_user = $zeile['dev_user'];        
        }
        if($is_user == $fromUsername)
        {
            $this->responseAnyMessage($socket, "try later.");
            return;
        }
        else
        {
            // trage Username in DB ein
            
            
            // do playRoulette
            return true;
        }
        
        // response
        
            echo "usernameArray:\n";
            print_r($this->usernameArray);
            echo "\n";
            echo "fromUsername:\n";
            print_r($fromUsername); 
            
            $search = in_array($fromUsername, $this->usernameArray);
            if($search === true){
                return $search;
            }  else {
                array_push($this->usernameArray, $fromUsername);
            }
        
    }
    
    /*
     * @return String
     */
    public function openDatabase(){
        try
        {
            $db = new PDO(DB_HOST.DB_NAME,DB_USER,DB_PWD);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
            die();
        }
        return $db;
    }
    
    
    public function kickUser($socket, $command, $from) {
        
        $array = array( 
                    "Command"=>"KICK",
                    "Data"=>"$from kicked"
                    );
        $json_data = json_encode($array);
           
        $type = new serverPackets();
        $this->SocketSendData($socket, $type->ChatCommand, $json_data);
 
    }
    
    public function autoKickUserFromChannel($socket, $from) {
        if($from == AUTOKICK_USERNAME)
        {
            $array = array( 
                        "Command"=>"KICK",
                        "Data"=>"$from you are currently not allowed to join this Channel!"
                        );
            $json_data = json_encode($array);

            $type = new serverPackets();
            $this->SocketSendData($socket, $type->ChatCommand, $json_data);
        }    
    }
    
    /* 
     * Bekomme Userlist in Array
     */
    public function getUserList(){
        
    }
}