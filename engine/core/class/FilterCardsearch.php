<?php
class FilterCardsearch
{

    // Karten Filter
    public $filter_cardname;
    public $filter_kartenwahl_dropdown;
    public $filter_monster_dropdown;
    public $filter_attribute;
    public $filter_race;
    public $filter_level;
    public $filter_atk;
    public $filter_def;


    // Kartenspeicher array von abfrage
    public $result_id;
    public $result_name;
    public $result_desc;
    public $result_attribute;
    public $result_race;
    public $result_level;
    public $result_atk;
    public $result_def;
    public $result_wiki_cardruling_link;

    public $wiki_query; // speichert ids im string der suchanfrage fuer query der en cards

    /*
    $type_monster = array(
                "1" => "Monster",
                "17" => "Normales-Monster",
                "33" => "Effekt-Monster",
                "97" => "Fusion-Monster",
                "161" => "Ritual Monster",
                "256" => "Fallenmonster",
                "545" => "Spirit Monster",
                "1057" => "Union Monster",
                "2048" => "Dual",
                "4113" => "Normales Empf&auml;nger Monster" );
                "4129" => "Empf&auml;nger Monster",
                "8193" => "Normales Synchro Monster",
                "8225" => "Effekt Synchro Monster",
                "2097185" => "Flip Effekt-Monster",
                "4194337" => "Toon Monster",
                "8388641" => "XYZ Effekt-Monster",
                "94944637" => "Zwilling Monster",
                );



  $type_spell = array(
                "2" => "Zauber",
                "130" => "Ritual Zauber",
                "65538" => "Schnellspiel-Zauber",
                "131074" => "Permanent-Zauber",
                "262146" => "Ausr&uuml;stung-Zauber",
                "524290" => "Feld-Zauber",
                );


  $type_trap = array(
                "4" => "Falle",
                "131076" => "Falle Permanent",
                "1048580" => "Konter Falle",
                );


                */

    // Filter Vars setzen mit Post Daten
    function SetFilter()
    {

        if(!empty($_POST['filter_cardname']))
        {
            $this->filter_cardname = sqlite_escape_string($_POST['filter_cardname']);
        }
        if($_POST['filter_kartenwahl_dropdown'] > 0)
        {
            // pruefen was gesucht wird
            switch($_POST['filter_kartenwahl_dropdown'])
            {
                case "1": // Monster
                    $this->filter_kartenwahl_dropdown = (" ('1', '17', '33', '97', '161', '256', '545', '1057', '2048', '4113', '4129', '8193', '8225', '2097185', '4194337', '8388641', '94944637') "); // Type Monster zahlen adden
                    break;

                case "2": // Spell
                    $this->filter_kartenwahl_dropdown = (" ('2', '130', '65538', '131074', '262146', '524290') ");
                    break;
                case "4": // Trap
                    $this->filter_kartenwahl_dropdown = (" ('4', '131076', '1048580') ");
                    break;

                case "5":
                    $this->filter_kartenwahl_dropdown = (" ('1', '17', '33', '97', '161', '256', '545', '1057', '2048', '4113', '4129', '8193', '8225', '2097185', '4194337', '8388641', '94944637', '2', '130', '65538', '131074', '262146', '524290', '4', '131076', '1048580') ");
                    break;
            }

        }
        if($_POST['filter_monster_dropdown'] == true)
        {
            switch($_POST['filter_monster_dropdown'])
            {
                case "5": // alle
                    break;
                case "17": // Normales Monster
                    $this->filter_monster_dropdown = ("
                                                AND
                                                  datas.type = '17'
                                                ");
                    break;
                case "33": // Effekt Monster
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '33'
                                                ");
                    break;
                case "97": // Fusion Monster
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '97'
                                                   ");
                    break;
                case "161": // Ritual Monster
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '161'
                                                   ");
                    break;
                case "8225": // Effekt Synchro Monster
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '8225'
                                                   ");
                    break;
                case "8193": // Normales Synchro Monster
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '8193'
                                                   ");
                    break;
                case "8388641": // XYZ
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '8388641'
                                                   ");
                    break;
                case "4129": // Empfaenger Monster
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '4129'
                                                   ");
                    break;
                case "94944637": // Zwilling Monster
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '94944637'
                                                   ");
                    break;
                case "1057": // Union Monster
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '1057'
                                                   ");
                    break;
                case "545": // Spirit Monster
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '545'
                                                   ");
                    break;
                case "2097185": // Flip Effekt Monster
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '2097185'
                                                   ");
                    break;
                case "4194337": // Toon Monster
                    $this->filter_monster_dropdown =  ("
                                                AND
                                                  datas.type = '4194337'
                                                   ");
                    break;
            }


        }
        if($_POST['filter_attribute'] != "5")
        {
            switch($_POST['filter_attribute'][0])
            {
                case "1": // Erde
                    $this->filter_attritbute = ("
                                         AND
                                           datas.attribute = '1'
                                         ");
                    break;
                case "2": // Wasser
                    $this->filter_attritbute = ("
                                         AND
                                           datas.attribute = '2'
                                         ");
                    break;
                case "4": // Feuer
                    $this->filter_attritbute = ("
                                         AND
                                           datas.attribute = '4'
                                         ");
                    break;
                case "8": // Wind
                    $this->filter_attritbute = ("
                                         AND
                                           datas.attribute = '8'
                                         ");
                    break;
                case "16": // Licht
                    $this->filter_attritbute = ("
                                         AND
                                           datas.attribute = '16'
                                         ");
                    break;
                case "32": // Finsternis
                    $this->filter_attritbute = ("
                                         AND
                                           datas.attribute = '32'
                                         ");
                    break;
            }
        }
        if($_POST['filter_race'] != "5")
        {
            switch($_POST['filter_race'])
            {
                case "1": // Krieger
                    $this->filter_race = ("
                                 AND
                                   datas.race = '1'
                                 ");
                    break;
                case "2": // Fee
                    $this->filter_race = ("
                                 AND
                                   datas.race = '2'
                                 ");
                    break;
                case "4": // Hexer
                    $this->filter_race = ("
                                 AND
                                   datas.race = '4'
                                 ");
                    break;
                case "8": // Unterweltler
                    $this->filter_race = ("
                                 AND
                                   datas.race = '8'
                                 ");
                    break;
                case "16": // Zombie
                    $this->filter_race = ("
                                 AND
                                   datas.race = '16'
                                 ");
                    break;
                case "32": // Maschine
                    $this->filter_race = ("
                                 AND
                                   datas.race = '32'
                                 ");
                    break;
                case "64": // Wasser
                    $this->filter_race = ("
                                 AND
                                   datas.race = '64'
                                 ");
                    break;
                case "128": // PYro
                    $this->filter_race = ("
                                 AND
                                   datas.race = '128'
                                 ");
                    break;
                case "256": // Fels
                    $this->filter_race = ("
                                 AND
                                   datas.race = '256'
                                 ");
                    break;
                case "512": // Gefluegeltes Ungeheuer
                    $this->filter_race = ("
                                 AND
                                   datas.race = '512'
                                 ");
                    break;
                case "1024": // Pflanze
                    $this->filter_race = ("
                                 AND
                                   datas.race = '1024'
                                 ");
                    break;
                case "2048": // Insekt
                    $this->filter_race = ("
                                 AND
                                   datas.race = '2048'
                                 ");
                    break;
                case "4096": // Donner
                    $this->filter_race = ("
                                 AND
                                   datas.race = '4096'
                                 ");
                    break;
                case "8192": // Drache
                    $this->filter_race = ("
                                 AND
                                   datas.race = '8192'
                                 ");
                    break;
                case "16384": //Ungeheuer
                    $this->filter_race = ("
                                 AND
                                   datas.race = '16384'
                                 ");
                    break;
                case "32768": // Ungeheuer Krieger
                    $this->filter_race = ("
                                 AND
                                   datas.race = '32768'
                                 ");
                    break;
            }
        }
        if($_POST['filter_level'] != "")
        {
            switch($_POST['filter_level'])
            {
                case "1":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '1'
                                 ");
                    break;
                case "2":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '2'
                                 ");
                    break;
                case "3":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '3'
                                 ");
                    break;
                case "4":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '4'
                                 ");
                    break;
                case "5":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '5'
                                 ");
                    break;
                case "6":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '6'
                                 ");
                    break;
                case "7":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '7'
                                 ");
                    break;
                case "8":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '8'
                                 ");
                    break;
                case "9":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '9'
                                 ");
                    break;
                case "10":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '9'
                                 ");
                    break;
                case "11":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '10'
                                 ");
                    break;
                case "12":
                    $this->filter_level = ("
                                 AND
                                   datas.level = '1'
                                 ");
                    break;

                default:
                    exit;
            }
        }

        if($_POST['filter_atk'] != "")
        {
            $filter_atk_escaped = sqlite_escape_string($_POST['filter_atk']);
            $this->filter_atk = ("
                           AND
                             datas.atk = '$filter_atk_escaped'
                           ");
        }
        if($_POST['filter_def'] != "")
        {
            $filter_def_escaped = sqlite_escape_string($_POST['filter_def']);
            $this->filter_def = ("
                           AND
                             datas.def = '$filter_def_escaped'
                           ");
        }


    } // Ende Set Filter

    /*
     * Hier wird das Ergebnis der Suchanfrage in den Eigenschaften als array gespeichert
     */
    function GetSearchCardName($message)
    {
        $attribute = array(
        "0" => "",
        "1" => "Earth",
        "2" => "Water",
        "4" => "Fire",
        "8" => "Wind",
        "16" => "Light",
        "32" => "Dark",
        "64" => "Divine" );

        $race = array(
        "1" => "Warrior",
        "2" => "Spellcaster",
        "4" => "Fairy",
        "16" => "Zombie",
        "8" => "Unterweltler",
        "32" => "Maschine",
        "64" => "Aqua",
        "128" => "Pyro",
        "256" => "Rock",
        "512" => "Winged Beast",
        "1024" => "Plant",
        "2048" => "Insect",
        "4096" => "Thunder",
        "8192" => "Dragon",
        "16384" => "Beast",
        "32768" => "Beast-Warrior",
        "65536" => "Dinosaur",
        "131072" => "Fish",
        "262144" => "Sea Searpent",
        "524288" => "Reptile",
        "1048576" => "Psychic",
        "2097152" => "flying Beast"
    );


        $message = substr($message, 1);
        $db = new MyDB();

        $abfrage =
            ("
                  SELECT
                    texts.id, name, desc, datas.type, datas.attribute, datas.race, datas.level, datas.atk, datas.def
                  FROM
                    texts
                  LEFT JOIN
                    datas
                  ON
                    texts.id = datas.id
                  WHERE
                    texts.name LIKE '%$message%'
                  LIMIT
                    1
                ");

        $ergebnis = $db->query($abfrage) or die("Error in query: <span style='color:red;'>$abfrage</span>");

        // Counter fuer array setzen
        $result = '';
        while ($row = $ergebnis->fetchArray())
        {
            //$result .= $row['id'];
            //$result  .= utf8_decode($row['name']);
            $result  .= 'attr: ' . $attribute[$row['attribute']] . '  |  ';
            $result  .= 'race: ' . $race[$row['race']] . '  |  ';
            $result  .= 'level: ' . $row['level'] . '  |  ';
            $result  .= 'atk' . $row['atk'] . '  |  ';
            $result  .= 'def: ' . $row['def'];

        }
        $db->close();

       // $res = implode($result);
        return $result;
    }




} // end class

