<?php


class talkResponse
{
    public function detectRequest($message)
    {
        switch ($message)
        {
            case '!help':
                return '!yugioh, !decksize, !bestdeck, !yugi,';
            case '!yugioh':
                return 'its time for a duel!';
            case '!decksize';
                return 'A Deck with 40 cards';
            case '!yugi':
                return 'Best Player';

            default:
                return $this->getcard($message);
        }
    }

    public function getcard($card)
    {
        $result = new FilterCardsearch();
        return $result->GetSearchCardName($card);
    }
}