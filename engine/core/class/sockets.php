<?php
/*
 * Main Socket Class
 * Copyright 2015 Benjamin Knecht
 * 
 * Main Functions
 * opOpen() // Socket Connect to Server
 * onMessage // Listen Socket on incoming Messages
 * onError // Error Handling
 * onClose // Socket close Handling
 * 
 * Helper Functions
 * loadClientPacketsType() // Detect the sended Paket Type from Server
 * receive() // Runs in onMessage and detect if by socket_recv Disconnect or Crash
 */
class sockets
{
    public $socket;

    // connect to tcp Socket Server and send login
    public function onOpen($ip, $port) {
        try {
            $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
            $result = socket_connect($this->socket, $ip, $port); // try Socket connect
            
                echo "Connected to " . $ip."|".$port;
        } catch (Exception $ex) {
            socket_strerror(socket_last_error());
        }
    }
    
    public function onMessage() {
       $data = $this->receive($this->socket);
       
       // still connected and waiting for Data do nothing and return
       if($data === FALSE){
           return; 
       }
       
       // Try Reconnect, login, join channel and Send a Msg "Impossible to Kick Chatbot!"
       if($data == 'Disconnected')
       {
           return 'Disconnected';
       }
       /*
        * if Data not False, load PacketType Detect. Thats needed to load the right
        * onMessage Function
        */
       $this->loadClientPacketsType($data);
                    
                    /*
                     * CONSOLE DEBUG INCOMING DATA
                     */
                    // cut binary Data um Json Daten dekodieren zu können.
                    $cut = substr("$data", 3);
                    $type = unpack('c', $data);
                    $jsonDecode = json_decode($cut);
                       
                        echo "\n";
                        var_dump($type);
                        echo "\n";
                        var_dump($jsonDecode);
                        echo "\n"; 
                        
                        echo "\nDEBUG BUFFER \n";
                        var_dump($data);
                        echo "\n"; 
        
    }
    
    // add reconnect
    public function onClose() {
        if($this->socket === FALSE)
        {
            // Try autoReconnect
            $this->onOpen(IP, PORT);
            $this->sendLogin($this->socket);
            $this->joinChannel($this->socket, CHANNEL);
        }
    }
    // php Errors stored in LOG, add exeption handler here
    public function onError() {

    }
   
    /*
     * unpack the first binary from Message to detect the sended Type
     */
   public function loadClientPacketsType($buffer){
       //$clientpackets = new clientPackets();
       $chatbot = new chatbot();
       $type = unpack('c', $buffer);
       
       $cut = substr("$buffer", 3);
       $jsonDecode = json_decode($cut);
       
       switch ($type[1]) {
           case 1:
           case 2: // Update Players
               echo "Update Players \n";
               break;
           case 3: // Login ok
               echo "Login ok \n";
               break;
           case 4: // Login Failed
               echo "Login Failed \n";
               break;
           case 5: // Server Message
               $chatbot->ServerMessage($this->socket, $buffer);
               break;
           case 6:    
           case 7:
           case 10: // Pong
           case 11: // RoomStart    
           case 12: // UserList
               echo "Userlist:\n";
               break;
           case 13: // Update User Info
               echo "Update User Info:\n";
               break;
           case 16: // JoinChannelAccept
               echo "Join Channel Accept \n";
               break;
           case 17: // Message
               $chatbot->loadChatResponse($this->socket, $buffer);
               break;
           case 18: // Duel Request
               echo "DuelRequest: \n";
               $chatbot->DuelRequest($this->socket, $buffer);
               break;
           case 25: // Start Duel
               echo "startDuel: \n";
               // send Data to ygocore Server?
               //var_dump($jsonDecode);
               $chatbot->startDuel($this->socket, $jsonDecode->duelformatstring, $jsonDecode->server);
               break;
           case 37: // CreateRoom
               echo "CreateRoom:\n";
               break;
           case 39: // Update User Info
               echo "AddChannelUser:\n";
               echo $jsonDecode->Users[0]->username;
               echo "\n";
               $chatbot->autoKickUserFromChannel($this->socket, $jsonDecode->Users[0]->username);
               break;
           case 40: // Update User Info
               echo "RemoveChannelUser:\n";
               //echo $jsonDecode->Users[0]->username;
               echo "\n";
               break;
           case 42: // Invalid
               echo "Invalid: \n";
               break;
           
           default:
               echo "Type Detceted: ".$type[1]." \n";
               break;
       }
   }
   
   /*
    * SocketSendData($socket, $type, $data)
    * Socket = $this->socket
    */
   public function SocketSendData($socket, $type, $data) {
       if($type > 9)
           {
           $send = socket_write($socket, pack('c', $type), 2);
           }
        else {
              $send = socket_write($socket, pack('c', $type), 1);
              }
         if ($send === false) {
            echo "socket_write() fehlgeschlagen. Grund: " . socket_strerror(socket_last_error($this->socket));
        }
        socket_write($socket, pack('S', strlen($data)), 2);
        socket_write($socket, $data, strlen($data)); 
   }
   
   public function SocketSendCommand($socket, $type) {
       if($type > 9)
           {
           $send = socket_write($socket, pack('c', $type), 2);
           }
        else {
              $send = socket_write($socket, pack('c', $type), 1);
              }
         if ($send === false) {
            echo "socket_write() fehlgeschlagen. Grund: " . socket_strerror(socket_last_error($this->socket));
        }
        
   }
   
   public function onDisconnect($data) {
            socket_close($this->socket);
        
            $this->onOpen(IP, PORT);
            $this->sendLogin($this->socket);
            $this->joinChannel($this->socket, CHANNEL);
        
    }
   
   public function receive($socket)
{
    // !
    // on all following cases we assume that
    // socket_select() returned the current socket as "changed"
    // !

    $timeout = 3; // set your timeout

    /* important */
    $socket_recv_return_values['no_data_received'] = false;
    $socket_recv_return_values['client_disconnected'] = 0;

    $start = time();
    $received_data = null;
    $received_bytes = null;
    socket_set_nonblock($socket);
    socket_clear_error();
    while(
        ($t_out=((time()-$start) >= $timeout)) === false
        and ($read=@socket_recv($socket, $buf, 4096, 0)) >= 1
    ){
        $received_data  = (isset($received_data)) ? $received_data . $buf : $buf;
        $received_bytes = (isset($received_bytes)) ? $received_bytes + $read : $read;
    }
    $last_error = socket_last_error($socket);
    socket_set_block($socket);

    if($t_out === true){
        throw new Exception(
            'timeout after ' . ((!$received_bytes) ? 0 : $received_bytes) . ' bytes',
            0 // your eCode here
        );
    }
    elseif($last_error !== false and $last_error !== 0){
        throw new Exception(
            socket_strerror($last_error),
            $last_error
        );
    }
    else{
        if($read === $socket_recv_return_values['no_data_received']){
            // client returned NO DATA
            // but we were in a loop and could have got some data before:
            if($received_bytes < 1){
                // client is connected but sent NO DATA ?
                // no:
                // in this case the client must be "crashed" because -
                // it is not possible to "send no data" (zero bytes)
                // socket_select() now returns this socket as "changed" "forever"
                /*throw new Exception(
                    'client crashed',
                    0 // your eCode here
                );*/
                // No Data return to Listen again
                return FALSE;
            }else{
                // client returned DATA
                return $received_data;
            }
        }
        elseif($read === $socket_recv_return_values['client_disconnected']){
            // client disconnected
            if($received_bytes < 1){
                // return 'Disconnected';
                // client disconnected before/without sending any bytes
                throw new Exception(
                    'client disconnected',
                    0 // your eCode here
                ); 
            }
            else{
                // *this value* ^= $socket_recv_return_values['client_disconnected']
                //
                // client disconnected AFTER sending data (we were in a loop!)
                // socket_select() will return this socket "forever" as "changed" and -
                // socket_recv() will return *this value* "forever".
                // we will be "back" again "very soon" to see:
                //  socket_recv() returns *this value* AND no bytes received
                //  which results in disconnect-exception above
                return $received_data;
            }
        }
    }
}

public function onMessageDuelServer() {
       $data = $this->receive($this->socket);
       
       // still connected and waiting for Data do nothing and return
       if($data === FALSE){
           return; 
       }
       
       // Try Reconnect, login, join channel and Send a Msg "Impossible to Kick Chatbot!"
       if($data == 'Disconnected')
       {
           return 'Disconnected';
       }
       /*
        * if Data not False, load PacketType Detect. Thats needed to load the right
        * onMessage Function
        */
       $this->loadClientPacketsType($data);
                    
                    /*
                     * CONSOLE DEBUG INCOMING DATA
                     */
                    // cut binary Data um Json Daten dekodieren zu können.
                        
                        echo "\nDEBUG BUFFER \n";
                        var_dump($data);
                        echo "\n"; 
        
    }
}