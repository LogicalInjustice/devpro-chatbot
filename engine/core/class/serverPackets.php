<?php

class serverPackets
{
    public $GameList = 0;
    public $ChannelList = 1; // Login Request
    public $RandomSpectate = 2;
    public $Ping = 3;
    public $Login = 4;
    public $Register = 5;
    public $UserList = 6;
    public $ChatMessage = 8;
    public $JoinChannel = 9;
    public $LeaveChannel = 10;
    public $AddFriend = 12;
    public $RemoveFriend = 13;
    public $RequestDuel = 14;
    public $AcceptDuel = 15;
    public $RefuseDuel = 16;
    public $DevPoints = 17;
    public $ChatCommand = 18;
    public $DevPointCommand = 19;
    public $TournamentList = 20;
    public $TournamentStart = 21;
    public $TournamentJoin = 22;
    public $TournamentLeave = 23;
    public $TournamentIsReady = 24;
    public $TournamentCreate = 25;
    public $Stats = 26;
    public $TeamStats = 27;
    public $TeamCommand = 28;
    public $UpdatePassword = 29;
    public $SpectateUser = 30;
    public $HostDuel = 31;
    public $GetRanking = 32;
    public $Validate = 33;
    public $Resend = 34;
    public $UpdateEmail = 35;
    public $JoinQueue = 36;
    public $LeaveQueue = 37;
    public $AcceptMatch = 38;
    public $RefuseMatch = 39;
    
}