<?php
/*
 * @Copyright Benjamin Knecht
 * Chatbot App which connect to a CSharp/C# Chat.
 */

// DEB
error_reporting(E_ALL);

require 'config.php';
require 'autoloader.php';
class Programm {
    
    /*
     * BaseClass Konstruktor
     */
    function __construct() {

    }
    
    public function run() {
         $socket = new sockets();
         $chatbot = new chatbot();
         $socket->onOpen(IP,PORT); 
         $chatbot->sendLogin($socket->socket, USERNAME, PASSWORD);
         $chatbot->joinChannel($socket->socket, CHANNEL);

        while(true) {
           $socket->onMessage();
           //$chatbot->autotalk($socket->socket); // be carefully, the Bot goes crazy ;-)
        }
    }
}
$programm = new Programm();
$programm->run();



