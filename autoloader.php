<?php
/*
 * Autoloader
 * Lädt die benötigten Klassen für Objekte um nicht immer manuell alle Klassen einbinden zu müssen.
 */
function __autoload ($klasse) {
  // die bösesten zeichen in klassennamen mal sicherheitshalber verbieten
  if (strpos ($klasse, '.') !== false || strpos ($klasse, '/') !== false
      || strpos ($klasse, '\\') !== false || strpos ($klasse, ':') !== false) {
    return;
  }
  if (file_exists ('engine/core/class/'.$klasse.'.php')) {
    include_once 'engine/core/class/'.$klasse.'.php';
  }
}
 
